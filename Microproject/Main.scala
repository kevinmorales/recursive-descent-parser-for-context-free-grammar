import scala.util.matching.Regex

// Unambiguous grammar
// E -> T + E | T
// T -> Const | Var

// Easier to parse grammar
// S -> E$
// E -> Terminal E2
// E2 -> + E
// E2 -> NIL
// Terminal -> Const
// Terminal -> Val

/*********
 *  Original Grammar
 *         S -> E$
 *         E -> Terminal E2
 *        E2 -> + E || NIL
 *  Terminal -> Const || Var
 */

/**********
 *  New Grammar
 *         S -> E$
 *         E -> T E2
 *        E2 -> + E || NIL
 *         T -> Terminal T2
 *        T2 -> * Terminal || NIL
 *  Terminal -> Const || Var
 */


abstract class S {
  def eval(env: Main.Environment): Int
}
abstract class Terminal extends S

case class E(left: T, right: Option[E2]) extends S {
  def eval(env: Main.Environment): Int = {
    val a1: Int = left.eval(env)

    right match {
      case Some(right) => a1 + right.eval(env)
      case None => a1
    }

  }//END eval

}//END class E

case class T(left: Terminal, right: Option[T2]) extends S{
  def eval(env: Main.Environment): Int = {

    val a1: Int = left match {
      case v: Var => v.eval(env)
      case c: Const => c.eval(env)
    }
    right match {
      case Some(right) => a1 * right.eval(env)
      case None => a1

    }

  }//END eval

}//END class T

case class T2(left: T) extends S{

  def eval(env: Main.Environment): Int = left.eval(env)

}//END class T2

case class E2(left: E) extends S {

  def eval(env: Main.Environment): Int = left.eval(env)

}//END class E2

case class Var(n: String) extends Terminal {

  //Parameter: x,y bindings Returns: integer
  def eval(env: Main.Environment): Int = env(n)

}//END class Var

case class Const(v: Int) extends Terminal {

  def eval(env: Main.Environment): Int = v

}//END class Const

class RecursiveDescent(input:String) {
  val constregex: Regex = "^[0-9]+".r
  val varregex: Regex = "^[A-Za-z]+".r

  var index = 0

  def parseS(): S = parseE()

  def parseE(): E = E(parseT(), parseE2())
  def parseT(): T = T(parseTerminal(), parseT2())

  def parseT2(): Option[T2] = {
    if(index < input.length && input(index) == '*'){
      index += 1
      Some(T2(parseT()))
    }
    else None
  }

  def parseE2(): Option[E2] = {
    if (index < input.length && input(index) == '+'){
      index+=1// Advance past +
      Some(E2(parseE()))
    }
    else None
  }

  def parseTerminal(): Terminal = {
    // Get the unparsed part of the string.
    val currStr = input.substring(index)

    // Get either the const or var which is there.
    val consts = constregex.findAllIn(currStr)
    if (consts.hasNext){
      val const: String = consts.next()
      index += const.length()
      Const(const.toInt)
    }
    else {
      val vars = varregex.findAllIn(currStr)
      val varname = vars.next()
      index += varname.length()
      Var(varname)
    }
  }
}

object Main {
  type Environment = String => Int

  def main(args: Array[String]){
    val env: Environment = { case "x" => 5 case "y" => 7 case "z"=>2}

    val rd = new RecursiveDescent("10+x+y+2*5*z")
    val exp2rd:S = rd.parseE()
    println(exp2rd)
    println(exp2rd.eval(env))
  }
}

