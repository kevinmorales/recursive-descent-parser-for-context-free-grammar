import scala.io.StdIn.readLine

//Class definitions
abstract class S
abstract class A extends S
case class E(left: T, right: Option[E2]) extends S
case class E2(left: E3) extends S
case class E3(left: T, right: Option[E2]) extends S
case class T(left: F, right: Option[T2]) extends S
case class T2(left: F, right: Option[T2]) extends S
case class F(left: A, right: Option[F2]) extends S
case class F2(left: Option[F2]) extends S
case class A2(left: E) extends A
case class C(left: Char) extends A

/**************************************
  TEST 1
  pattern? ((h|j)ell. worl?d)|(42)
  string? hello world
  match
  string? jello word
  match
  string? jelly word
  match
  string? 42
  match
  string? 24
  no match
  string? hello world42
  no match

  TEST 2
  pattern? I (like|love|hate)( (cat|dog))? people
  string? I like cat people
  match
  string? I love dog people
  match
  string? I hate people
  match
  string? I likelovehate people
  no match
  string? I people
  no match
 *****************************************/
object Main {

  def main(args: Array[String]): Unit  = {
    //local constants

    //local variables
    var pattern: String = ""
    var string: String = ""
    /*******************/

    //Prompt the user for a pattern
    pattern = readLine("pattern? ")

    //Create a parse tree from the given pattern
    val parseTree = new ParseTree(pattern).parseS()

    //Prompt the user for a string to match against the pattern
    string = readLine("string? ")

    //WHILE the user does not enter an empty string
    while (string != "") {

      //If the evaluation returned a match
      if (new PatternMatcher(string, parseTree).checkForMatch())
          //Let user know it was a match
          println("match")

      //ELSE the evaluation did not return a match
      else
          //Let user know it was not a match
          println("no match")
      //END IF

      //Prompt the user for another string
      string = readLine("string? ")

    }//END WHILE
  }//END main
}//END Main class

/********************************************************
 * @param input - user given string that will be checked
 *              against a parse tree to confirm a match
 * @param tree - an S type tree structure that represents
 *                  a grammar built from a given pattern
 *                  from the user.
 ******************************************************/
class PatternMatcher(input: String, tree: S){
  //class constants
  //class variables
  var index: Int = 0
  var indexBeforeRecursion: Int = 0
  var rightResult: Boolean = false
  var leftResult: Boolean = false

  /*********************************************************
   *
   * @return - returns True if the given string parameter
   *         matches the given pattern from the user, False
   *         otherwise.
   ********************************************************/
  def checkForMatch(): Boolean = {

    //local constants
    //local variables
    var result: Boolean = false
    /**************************/

    //The result of the recursive descent, which initially takes the root node of the parse tree as a parameter
    result = recursiveDescent(tree)

    //IF not at end of input string after recursive descent
    if (index < input.length)
      //Return False
      false //Without this the "hello world42" case fails
    //ELSE we are at the end of the input string
    else
      //Return the result of the recursive descent
      result
    //END IF
  }

  /*******************************************************
   * @param node - a tree node from the parse tree built from the initial pattern
   *             all tree nodes are type S or an extension of S class
   * @return - a boolean that confirms a given string matches the given pattern
   *******************************************************/
  def recursiveDescent(node: Any): Boolean = node match { // recursion to break down the insides of each decent, returning booleans if match or not.

    //CASE node is type E
    case node: E =>
      //Save the current index doing any recursive calls
      indexBeforeRecursion = index
      //Evaluate left side with recursion
      leftResult = recursiveDescent(node.left)
      //Match right side node
      node.right match {
        //CASE there is a right side node
        case Some(rightNode) =>
          //IF left side evaluation returned False
          if (!leftResult) {
            //Reset index to what it was before left side recursion
            index = indexBeforeRecursion
            //Return the result of right side evaluation
            recursiveDescent(rightNode)
          }
          //ELSE there is a left side node (an E node or an OR)
          else
          //Return True
          true
        //CASE there is no right side node
        case None =>
          //Return the evaluation of the left side recursion
          leftResult
      }//END match
    //END CASE

    //CASE node is type E2
    case node: E2 => recursiveDescent(node.left)

    case node: E3 =>
      //Set index before performing any recursive evaluations
      indexBeforeRecursion = index
      //Get result of left side recursion
      leftResult = recursiveDescent(node.left)
      //Match right side node
      node.right match {
        //CASE there is a right side node
        case Some(rightNode) =>
          //IF left side evaluation returned False
          if (!leftResult) {
            //Reset index to what it was before left side recursion
            index = indexBeforeRecursion
            //Return the result of right side evaluation
            recursiveDescent(rightNode)
          }
          //ELSE there is a left side node (an E node or an OR)
          else
          //Return True
          true
        //END IF
        //CASE there is no right side node
        case None =>
          //Return the evaluation of the left side recursion
          leftResult
      }//END right side match

    //T  -> F and T2
    //CASE node is type T
    case node: T =>
      //Match right side node
      node.right match {
        //CASE there is a right side node
        case Some(rightNode) =>
          //Set index before performing recursive evaluation
          indexBeforeRecursion = index
          //If the right side recursive evaluation is valid
          if (recursiveDescent(rightNode))
          //Return True
          true
          //ELSE the right side recursion is invalid
          else
          {
            //Reset index to before recursion
            index = indexBeforeRecursion
            //Get result of left side recursion first
            leftResult = recursiveDescent(node.left)
            //Then get result of right side recursion
            rightResult = recursiveDescent(rightNode)
            //Return true only if the result of both evaluations is true
            leftResult && rightResult
          }
        //CASE there is no right side node
        case None => recursiveDescent(node.left)
        //Return result of left side recursion

      }//END match

    case node: T2 =>
      node.right match {
        case Some(rightNode) =>
          indexBeforeRecursion = index
          //if (node.left.right.nonEmpty && recursiveDescent(rightNode))
          if(lookAhead(node) && recursiveDescent(rightNode))
            true
          else {
            index = indexBeforeRecursion
            recursiveDescent(node.left) && recursiveDescent(rightNode)
          }//END IF
        case None => recursiveDescent(node.left)
      }

    //CASE node is of type F
    case node: F =>
      //Match right node
      node.right match {
        //CASE there is a right side node
        case Some(_:F2) =>
          //Set index before performing recursive evaluation
          indexBeforeRecursion = index
          //Get the result from left side recursion
          leftResult = recursiveDescent(node.left)
          //IF the result was false
          if (!leftResult)
          //Reset index back to current
          index = indexBeforeRecursion
          //END IF
          //Return True
          true
        //CASE there is no right side node
        case None => recursiveDescent(node.left)
      }

    //CASE node is of type F2
    case node: F2 => true
    //Return True because this node exists


    //CASE node is of type A2
    case node: A2 => recursiveDescent(node.left)

    //CASE node is of type C
    case node: C =>
      //IF current index is greater than the length of the input string
      if (index >= input.length)
        false
      //ELSE IF the left node is a "." then true, because a "." translates to any character in the grammar
      else if (node.left == '.') {
        //Increment index
        index += 1
        //Return True
        true
      }
      //ELSE IF the left node character is the same as the current node
      else if (input.charAt(index) == node.left) {
        //Increment the index
        index += 1
        //Return True
        true
      }
      //ELSE this is not a valid character
      else
      //Return False
      false
    //END IF

  }//END recursiveDescent function

  /**********************************
   * This function looks ahead at future nodes in the tree.
   * Without this function, the "42" case in the ((h|j)ell. worl?d)|(42)
   * grammar fails
   * @param node - a T2 node
   * @return Boolean - returns true if there is a valid node
   *         in the left child's right child location.
   *
   */
  def lookAhead(node: T2): Boolean={
    //Get left child node of T2 which would be an F type node
    val left: F = node.left
    //Get right child node
    val right: Option[F2] = left.right
    right.nonEmpty
  }//END lookAhead

}

/**************************************
 *
 * Given Grammar:
 * S  -> E$
 *
 * E  -> T  and E2
 *
 * E2 -> '|' and E3
 * E2 -> NIL
 *
 * E3 -> T and E2
 *
 * T  -> F and T2
 *
 * T2 -> F and T2
 * T2 -> NIL
 *
 * F  -> A and F2
 *
 * F2 -> '?' and F2
 * F2 -> NIL
 *
 * A  -> C
 * A  -> '(' and A2
 *
 * A2 -> E and ')'
 *
 * @param pattern - a user given pattern
 *                which will be parsed
 *                into a tree to check
 *                strings against
 **************************************/
class ParseTree (pattern: String){
  //class constants
  val length: Int = pattern.length() //Length of string parameter - pattern
  //class variables
  var current: Int = 0               //Current index of string, init to zero

  /************************/

  //S  -> E$
  def parseS(): S = parseE()

  //E  -> T  and E2
  def parseE(): E = E(parseT(), parseE2())

  //E2 -> '|' and E3   OR    E2 -> NIL
  def parseE2(): Option[E2]= {
    //IF not at end of string AND current character is not a closing parentheses
    if (current < length && pattern(current) == ')'){
      //Increment index counter
      current += 1
      //Return None because this is the end of an parentheses statement
      None
    }
    //ELSE IF not at end of string AND current character is an 'OR' bar
    else if (current < length && pattern(current) == '|'){
      //Increment index counter
      current += 1
      //Call function to parse E3 node then return an E2 node
      Some(E2(parseE3()))
    }
    //ELSE E2 is a NIL Node
    else
      None
    //END IF
  }//END parseE2

  //E3 -> T and E2
  def parseE3(): E3 = E3(parseT(), parseE2())

  // T  -> F and T2
  def parseT(): T = T(parseF(), parseT2())

  //T2 -> F and T2    OR   T2 -> NIL
  def parseT2(): Option[T2] = {
    //IF not at end of string AND current character is a  bar, question, or closing parentheses
    if(current < length && pattern(current) != '|' && pattern(current) != '?' && pattern(current) != ')')
      //Call function to parse F node and T2 node, then return an E2 node
      Some(T2(parseF(), parseT2()))
    //ELSE we received a NIL node
    else
      None
    //END IF
  }//END parseT2

  //F  -> A and F2
  def parseF(): F = F(parseA(), parseF2())

  //F2 -> '?' and F2   OR    F2 -> NIL
  def parseF2(): Option[F2] = {
     //IF not at end of string AND current character is a question
     if (current < length && pattern(current) == '?'){
       //Increment index counter
       current += 1
       //Call function to parse and return an F2 node
       Some(F2(parseF2()))
    }
    //ELSE we received a NIL node
    else
       None
    //END IF
  }//END parseF2

  //A  -> C    OR    A  -> '(' and A2
  def parseA(): A = {
    //IF the current character is an opening parentheses
    if (pattern(current) == '(') {
      //Increment the index counter
      current += 1
      //Call function to parse an E node and return the A2 node
      A2(parseE())
    }
    //ELSE this is a C Node
    else {
      //Increment the index counter
      current += 1
      //Return a C node
      C(pattern(current - 1))
    }//END IF

  }//END parseA

}//END createPattern class